package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Klass {
    public int number;
    protected List<Teacher> attachTeachers = new ArrayList<>();
    protected List<Student> attachStudents = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }


    public void setAttachTeachers(Teacher teacher) {
        this.attachTeachers.add(teacher);
    }


    public void setAttachStudents(Student student) {
        this.attachStudents.add(student);
    }

    public void assignLeader(Student student) {
        if (student.isIn(this)) {
            student.setLeader(true);
            for (Teacher attachTeacher : attachTeachers) {
                System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.", attachTeacher.name, this.number, student.name));
            }
            for (Student attachStudent : attachStudents) {
                System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.", attachStudent.name, this.number, student.name));
            }
        } else System.out.println("It is not one of us.");
    }

    public boolean isLeader(Student student) {
        return student.isIn(this) && student.isLeader();
    }

    public void attach(Teacher teacher) {
        if (teacher.belongsTo(this)) {
            setAttachTeachers(teacher);
        }
    }

    public void attach(Student student) {
        if (student.isIn(this)) {
            setAttachStudents(student);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

}
