package ooss;

public class Student extends Person {
    protected Klass klass;
    protected boolean isLeader = false;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public int getKlass() {
        return this.klass.getNumber();
    }

    public void setKlass(Klass klass) {
        this.klass = klass;
    }

    public boolean isLeader() {
        return isLeader;
    }

    public void setLeader(boolean leader) {
        isLeader = leader;
    }

    public String introduce() {
        if (this.klass == null) {
            return String.format("My name is %s. I am %d years old. I am a student.", getName(), getAge());
        } else if (isLeader()) {
            return String.format(
                    "My name is %s. I am %d years old. I am a student. I am the leader of class %d."
                    , getName(), getAge(), getKlass());
        } else {
            return String.format("My name is %s. I am %d years old. I am a student. I am in class %d.", getName(), getAge(), getKlass());
        }

    }

    public void join(Klass klass) {
        setKlass(klass);
    }

    public Boolean isIn(Klass klass) {
        return this.klass == klass;
    }

}
