package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    protected List<Klass> klassList = new ArrayList<>();

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    public List<Klass> getKlassList() {
        return klassList;
    }

    public void setKlassList(Klass klass) {
        this.klassList.add(klass);
    }

    public String introduce() {
//        return String.format("My name is %s. I am %d years old. I am a teacher. I teach Class %s.", getName(), getAge(), klassListToString());
        if (this.klassList.size() == 0) {
            return String.format("My name is %s. I am %d years old. I am a teacher.", getName(), getAge());
        } else {
            return String.format("My name is %s. I am %d years old. I am a teacher. I teach Class %s.", getName(), getAge(), klassListToString());
        }
    }

    public void assignTo(Klass klass) {
        setKlassList(klass);
    }

    public String klassListToString() {
        return this.klassList.stream().map(klass -> klass.getNumber() + "").collect(Collectors.joining(", "));
    }

    public boolean belongsTo(Klass klass) {
        return getKlassList().contains(klass);
    }

    public boolean isTeaching(Student student) {
        return getKlassList().stream().map(klass -> klass.getNumber()).collect(Collectors.toList()).contains(student.getKlass());
    }

}
