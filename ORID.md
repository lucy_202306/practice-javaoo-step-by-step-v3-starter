# Day3-ORID

## O

1. Learned Code Review,including what is it,the benefits of using it and how to use it.My group also use it to review yesterday's assignment.
2. Learned JAVA Stream API and got familiar with the use of filter,map and reduce through class exercises.I also learned other functions about JAVA Stream API,such as max, min, concat, and flatMap.
3. Learned what is Object, understand the three characteristics of object orientation: polymorphism, inheritance, and encapsulation, and get familiar with it through code exercises.

## R

I was fruitful.

## I

Why learn object Orientation? Improve program extensibility and code reuse rate, and facilitate the understanding of the relationship between classes.

## D

Improve my object-oriented development ideas.